//
//  LocMax.cpp
//  Triangles
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#include "LocMax.h"
#include "PATImage.h"

void LocMax::set_up(PATImage image, int kernelHalfSize, int nLocMax, float threshold)
{
    nlm = nLocMax;
    thr = threshold;
    
    locs = (Point2D *)malloc(nlm*sizeof(Point2D));
    
    im.set_up_with_data(NULL, image.width, image.height);
    GaussKernel kernel = set_up_kernel(kernelHalfSize);
    
    blur(image, kernel, im);
    im.normalize();
    
    clean_up_kernel(kernel);
}

void LocMax::clean_up(void)
{
    free(locs);
    im.clean_up();
}

void LocMax::find(void)
{
    // threshold should be in [0,1]
    std::vector<float> values;
    std::vector<Point2D> locations;
    for (int i = 1; i < im.height-1; i++) {
        for (int j = 1; j < im.width; j++) {
            float v = im.data[i*im.width+j];
            if (v > thr) {
                float vE = im.data[i*im.width+j+1];
                float vW = im.data[i*im.width+j-1];
                float vN = im.data[(i-1)*im.width+j];
                float vS = im.data[(i+1)*im.width+j];
                if (v > vE && v > vW && v > vN && v > vS) {
                    values.insert(values.end(), v);
                    locations.insert(locations.end(), Point2DMake(i, j));
                }
            }
        }
    }
    nLocs = nlm;
    if (values.size() < nLocs) {
        nLocs = (int)values.size();
    }
    for (int i = 0; i < nLocs; i++) {
        float max = -INFINITY;
        float jMax = 0;
        for (int j = 0; j < values.size(); j++) {
            float value = values.at(j);
            if (value > max) {
                max = value;
                jMax = j;
            }
        }
        locs[i] = Point2DMake(locations.at(jMax).x, locations.at(jMax).y);
        values.at(jMax) = 0.0;
    }
}

void LocMax::blur(PATImage input, GaussKernel kernel, PATImage output)
{
    int khs = floorf(kernel.size/2.0); // kernel halfsize
   
    PATImage padIm;
    padIm.set_up_with_data(NULL, input.width+2*khs, input.height+2*khs);
    
    for (int i = 0; i < input.height; i++) {
        for (int j = 0; j < input.width; j++) {
            padIm.data[(khs+i)*padIm.width+khs+j] = input.data[i*input.width+j];
        }
    }
    
    float acc;
    int indexPadded, indexKernel;
    for (int i = 0; i < input.height; i++) {
        for (int j = 0; j < input.width; j++) {
            acc = 0.0;
            for (int ii = -khs; ii <= khs ; ii++) {
                for (int jj = -khs; jj <= khs; jj++) {
                    indexPadded = (khs+i+ii)*padIm.width+khs+j+jj;
                    indexKernel = (khs+ii)*kernel.size+khs+jj;
                    acc += padIm.data[indexPadded]*kernel.data[indexKernel];
                }
            }
            output.data[i*output.width+j] = acc;
        }
    }
    
    output.normalize();
    padIm.clean_up();
}

GaussKernel LocMax::set_up_kernel(int halfSize)
{
    int size = 2*halfSize+1; // size should be odd
    
    GaussKernel kernel;
    kernel.size = size;
    kernel.data = (float *)malloc(size*size*sizeof(float));
    
    int i0 = size/2;
    int j0 = i0;
    float sd = (float)size/4.0; // standard deviation
    float variance = sd*sd;
    float sum = 0.0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float xdist = i-i0;
            float ydist = j-j0;
            float value = expf(-0.5*(xdist*xdist+ydist*ydist)/variance);
            kernel.data[i*size+j] = value;
            sum += value;
        }
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            kernel.data[i*size+j] /= sum;
        }
    }
    
//    PATImage image;
//    image.set_up_with_data(kernel.data, size, size);
//    image.normalize();
//    image.save_png_to_path("/Users/Cicconet/Desktop/Kernel.png");
//    image.clean_up();
    
    return kernel;
}

void LocMax::clean_up_kernel(GaussKernel kernel)
{
    free(kernel.data);
}

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
