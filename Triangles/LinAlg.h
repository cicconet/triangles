//
//  LinAlg.h
//  PairwiseMethods
//
//  Copyright © 2014 New York University.
//  See notice at the end of this file.
//

#ifndef LinAlg_h
#define LinAlg_h

#include <math.h>
#include <f2c.h>
#include <clapack.h>

typedef struct Matrix2D {
    float a11;
    float a12;
    float a21;
    float a22;
} Matrix2D;

typedef struct Point2D {
    float x;
    float y;
} Point2D;

typedef struct LineIntersection {
    Point2D point;
    float denominator; // different from zero if lines actually intersect
} LineIntersection;

inline Point2D Point2DMake(float x, float y)
{
    Point2D p; p.x = x; p.y = y; return p;
}

inline float norm(Point2D p)
{
    return sqrtf(p.x*p.x+p.y*p.y);    
}

inline Point2D svproduct(float scalar, Point2D p)
{
    return Point2DMake(scalar*p.x, scalar*p.y);
}

inline Point2D normalize(Point2D p)
{
    Point2D np = p;
    float n = norm(p);
    if (n > 0) {
        np = svproduct(1.0/n, p);
    }
    return np;
}

inline Point2D perp(Point2D p)
{
    return Point2DMake(-p.y, p.x);
}

inline Matrix2D matrix2d(float a11, float a12, float a21, float a22)
{
    Matrix2D M;
    M.a11 = a11;
    M.a12 = a12;
    M.a21 = a21;
    M.a22 = a22;
    return M;
}

inline Point2D mvproduct(Matrix2D M, Point2D p)
{
    return Point2DMake(M.a11*p.x+M.a12*p.y, M.a21*p.x+M.a22*p.y);
}

inline float dotproduct(Point2D p, Point2D q)
{
    return p.x*q.x+p.y*q.y;
}

inline Point2D vsum(Point2D p, Point2D q)
{
    return Point2DMake(p.x+q.x, p.y+q.y);
}

inline Point2D vnegative(Point2D p)
{
    return Point2DMake(-p.x, -p.y);
}

inline LineIntersection lineintersection(Point2D p, Point2D tauP, Point2D q, Point2D tauQ)
{
    LineIntersection li;
    li.denominator = tauQ.x*tauP.y-tauP.x*tauQ.y;
    if (li.denominator != 0) {
        float beta = (tauP.x*(q.y-p.y)+tauP.y*(p.x-q.x))/li.denominator;
        li.point = vsum(q, svproduct(beta, tauQ));
    }
    return li;
}

inline void eigdec(float * eigenValues, float * eigenVectors, float * input, int size)
{
	char jobz = 'V';						// compute eigenvalues and eigenvectors
	char range = 'A';						// all eigenvalues will be found
	char uplo = 'L';						// lower triangle of A is stored
	long int n = size;                   // the order of the matrix A
	long int lda = size;					// the leading dimension of the array A
	float vl = ' ';							// lower bound of the interval to be searched for eigenvalues (not used)
	float vu = ' ';							// upper bound of the interval to be searched for eigenvalues (not used)
	long int il = ' ';						// index of the smallest eigenvalues to be returned (not used)
	long int iu = ' ';						// index of the largest eigenvalue to be returned (not used)
	char sm[13] = "Safe minimum";
	float abstol = slamch_(sm);				// the absolute error tolerance for the eigenvalues
	long int m = n;							// the total number of eigenvalues found
	float * w = (float *)malloc(n*sizeof(float));
    // eigenvalues in ascending order
	float * z = (float *)malloc(n*n*sizeof(float));
    // the i-th column is the eigenvector associated with w(i)
	long int ldz = n;						// the leading dimension of the array z
	long int * isuppz = (long int *)malloc(2*m*sizeof(long int));
    // the support of the eigenvectors in z
	long int lwork = 26*n;                  // the dimension of the array WORK
	float * work = (float *)malloc(lwork*sizeof(float));
    // workspace
	long int liwork = 10*n;					// the dimension of the array IWORK
	long int * iwork = (long int *)malloc(liwork*sizeof(long int));
    // on exit, IWORK(1) returns the optimal LWORK
	long int info;
	
	int result;
	result = ssyevr_(&jobz,
                     &range,
                     &uplo,
                     &n,
                     input,
                     &lda,
                     &vl,
                     &vu,
                     &il,
                     &iu,
					 &abstol,
                     &m,
                     w,
                     z,
                     &ldz,
                     isuppz,
                     work,
                     &lwork,
                     iwork,
                     &liwork,
                     &info);
	
	if ((int)info == 0) {
        //		printf("Symmetric Eigenvalue Problem successfully solved.\n");
	} else if ((int)info < 0) {
		printf("Error. The %d argument had an illegal value.\n",(int)-info);
	} else {
        printf("Internal error.\n");
	}
	
	memcpy(eigenValues, w, n*sizeof(float));
    //	float * eigenVectors = z;
    for (int col = 0; col < size; col++) {
		for (int row = 0; row < size; row++) {
            eigenVectors[row*size+col] = z[col*size+row];
		}
	}
	
	free(iwork);
	free(work);
	free(isuppz);
	free(z);
	free(w);
}

#endif

//  Copyright © 2014 New York University.
//
//  All Rights Reserved. A license to use and copy this software and its documentation
//  solely for your internal research and evaluation purposes, without fee and without a signed licensing agreement,
//  is hereby granted upon your download of the software, through which you agree to the following:
//  1) the above copyright notice, this paragraph and the following paragraphs
//  will prominently appear in all internal copies;
//  2) no rights to sublicense or further distribute this software are granted;
//  3) no rights to modify this software are granted; and
//  4) no rights to assign this license are granted.
//  Please Contact The Office of Industrial Liaison, New York University, One Park Avenue, 6th Floor,
//  New York, NY 10016 (212) 263-8178, for commercial licensing opportunities,
//  or for further distribution, modification or license rights.
//
//  Created by Marcelo Cicconet.
//
//  IN NO EVENT SHALL NYU, OR ITS EMPLOYEES, OFFICERS, AGENTS OR TRUSTEES (“COLLECTIVELY “NYU PARTIES”)
//  BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY KIND ,
//  INCLUDING LOST PROFITS, ARISING OUT OF ANY CLAIM RESULTING FROM YOUR USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
//  EVEN IF ANY OF NYU PARTIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM OR DAMAGE.
//
//  NYU SPECIFICALLY DISCLAIMS ANY WARRANTIES OF ANY KIND REGARDING THE SOFTWARE,
//  INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF  MERCHANTABILITY
//  AND FITNESS FOR A PARTICULAR PURPOSE, OR THE ACCURACY OR USEFULNESS,
//  OR COMPLETENESS OF THE SOFTWARE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION,
//  IF ANY, PROVIDED HEREUNDER IS PROVIDED COMPLETELY "AS IS".
//  NYU HAS NO OBLIGATION TO PROVIDE FURTHER DOCUMENTATION, MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//  Please cite the following reference if you use this software in your research:
//
//  Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
//  Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
//  IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.
