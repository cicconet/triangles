Triangles

Software corresponding to the following research paper:
Marcelo Cicconet, Davi Geiger, Kristin Gunsalus, and Michael Werman.
Mirror Symmetry Histograms for Capturing Geometric Properties in Images.
IEEE Conference on Computer Vision and Pattern Recognition. Columbus, Ohio. 2014.

See main.cpp for usage examples.

Should be cross platform, although it was only tested on a Mac.

If you use Mac, though, it's better to use Triangles_Mac instead:
https://bitbucket.org/cicconet/triangles_mac

This project requires files from the following projects:
PAT, available at https://github.com/cicconet/PAT
Triangles_Mac, available at https://bitbucket.org/cicconet/triangles_mac

It also uses the libraries libpng, blas and lapack:
http://www.libpng.org
http://www.netlib.org/blas/
http://www.netlib.org/lapack/

To compile on Terminal:

g++
*.cpp
../../PAT/PAT/PATImage.cpp
../../PAT/PAT/PATWavelet.cpp
../../PAT/PAT/PATConvolution.cpp
../../PAT/PAT/PATCoefficients.cpp
../../Triangles_Mac/Triangles/main.cpp
../../Triangles_Mac/Triangles/EFT.cpp
../../Triangles_Mac/Triangles/Triangle.cpp
../../Triangles_Mac/Triangles/Triangles.cpp
-o triangles
-I/usr/local/include
-I../../PAT/PAT
-I../../Triangles_Mac/Triangles
-I../../Libraries/CLAPACK-3.2.1/INCLUDE
-L/opt/local/lib
-L../../Libraries/CLAPACK-3.2.1
-L../../Libraries/CLAPACK-3.2.1/F2CLIBS
-lpng
-lclapack

(or similar, depending on where your files and libraries are installed).

Marcelo Cicconet
New York University
marceloc.net
